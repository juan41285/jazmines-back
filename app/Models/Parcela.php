<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\SoftDeletes;

class Parcela extends BaseModel
{
    protected $table = 'parcelas';
    protected $guarded = [];
    public $timestamps = false;


    public function terrenos()
    {
        return $this->belongsToMany(Terreno::class);
        return $this->belongsToMany(Cliente::class, 'clientes_terrenos', 'terreno_id', 'cliente_id');

    }
}
