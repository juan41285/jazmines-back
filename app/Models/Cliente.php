<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends BaseModel
{
    protected $table = 'clientes';
    protected $guarded = [];
    public $timestamps = false;


    public function terrenos()
    {
        return $this->hasMany(Terreno::class);
        // return $this->belongsToMany(Terreno::class, 'cliente_terreno', 'cliente_id', 'terreno_id');

    }
    public function pagos(){
        return $this->hasMany(Pago::class);
    }
}
