<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\SoftDeletes;

class Terreno extends BaseModel
{
    protected $table = 'terrenos';
    protected $guarded = [];
    public $timestamps = false;


    public function parcela()
    {
        return $this->hasOne(Parcela::class);


    }
    public function clientes()
    {
        return $this->belongsTo(Cliente::class);
        // Si el nombre de la tabla es diferente a lo predeterminado o el ID de la tabla tiene otro nombre.
        // return $this->belongsToMany(Cliente::class, 'cliente_terreno', 'cliente_id', 'terreno_id');
    }
}
