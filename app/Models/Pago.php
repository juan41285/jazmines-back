<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\SoftDeletes;

class Pago extends BaseModel
{
    protected $table = 'pagos';
    protected $guarded = [];
    public $timestamps = false;


    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
        

    }
}
