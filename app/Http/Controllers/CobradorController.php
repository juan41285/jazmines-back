<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Terreno;
use App\Models\Pago;

class CobradorController extends Controller
{
    public function setCobrador(Request $request)
    {

        $cobrador_id = $request->cobrador_id;
        $cliente_id = $request->cliente_id;
    
       
    }
  public function getCobradores(){
        $existe = DB::table('cobradores')
                  ->leftJoin('cliente_cobrador','cliente_cobrador.cobrador_id', 'cobradores.id')
                  ->leftJoin('clientes', 'cliente_cobrador.cliente_id', 'clientes.id')
                  ->select('cobradores.*', DB::raw("count(clientes.id) as clientes"))
                  ->groupBy('cobradores.id')
                  ->get();

        if (count($existe)) {
            return $this->crearRespuesta($existe, 200);
        }else{
            return $this->crearRespuesta([], 200);
        }
    }
   public function addCobrador(Request $request){
       
       $nombre = $request->nombre;
       $dni = $request->dni;
       $telefono = $request->telefono;
       $direccion = $request->direccion;
    
       

       $existe = DB::table('cobradores')
            ->where('dni', '=', $dni)
            ->get();
       
       if(count($existe)){
         return $this->crearRespuesta($existe, 200);
       }
       else{
           $cobradores = DB::table('cobradores')->insertGetId(
                            [
                                'nombre' => $nombre,
                                'dni' => $dni,
                                'telefono' => $telefono,
                                'direccion' => $direccion
                            
                            ]
                        );

    if($cobradores){
          $nuevo = DB::table('cobradores')
            ->where('id', '=', $cobradores)
            ->first(); 
           return $this->crearRespuesta($nuevo, 201);
       }
       else{
           return $this->crearRespuesta(0, 500);
       }
       }

     
                        
   }
   public function editarCobrador(Request $request){
       
       $id = $request->id;
       $nombre = $request->nombre;
       $dni = $request->dni;
       $telefono = $request->telefono;
        $direccion = $request->direccion;
   
       
   
           $cliente = DB::table('clientes')->where('id', $id)->update(
                            [
                                'nombre' => $nombre,
                                'dni' => $dni,
                                'telefono' => $telefono,
                                'direccion' => $direccion
                            
                            ]
                        );

   
           return $this->crearRespuesta($id, 201);
   
       

     
                        
   }
}
