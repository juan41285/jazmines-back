<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ParcelasController extends Controller
{

   public function editarParcelas(Request $request){
   
    $id = $request->id;
    $contado = $request->contado;
    $cuota6 = $request->cuota6;
    $cuota12 = $request->cuota12;
    $espacios = $request->espacios;
    $sellado = $request->sellado;
    $comunal = $request->comunal;
    $bonificacion = $request->bonificacion;
   
    $parcela = DB::table('parcelas')
            ->where('id', $id)
            ->update([
                'contado' => $contado,
                'cuota6' => $cuota6,
                'cuota12' => $cuota12,
                'espacios' => $espacios,
                'sellado' => $sellado,
                'comunal' => $comunal,
                'bonificacion' => $bonificacion,
            ]);
 return $this->crearRespuesta($id, 200);

   } 

  public function guardarGastos(Request $request){

    $parcelas = DB::table('parcelas')->get();
    $mantenimiento_comun = $request->mantenimiento_comun;
    $mantenimiento_placa = $request->mantenimiento_placa;
    $placa = $request->placa;
    $inscripcion = $request->inscripcion;
    $excavacion = $request->excavacion;
    $expensas = $request->expensas;
    $cuota6 = $request->cuota6;
    $cuota12 = $request->cuota12;
   
  foreach ($parcelas as $parcela) {
     $id = $parcela->id;
     $gastos = DB::table('parcelas')
            ->where('id', $id)
            ->update([
                'mantenimiento_comun' => $mantenimiento_comun,
                'mantenimiento_placa' => $mantenimiento_placa,
                'excavacion' => $excavacion,
                'placa' => $placa,
                'inscripcion' => $inscripcion,
                'expensas' => $expensas,
                'cuota6' => $cuota6,
                'cuota12' => $cuota12,
               
            ]);
  }
    
 return $this->crearRespuesta($id, 200);

   } 
   public function getParcelas(){
       

       $parcelas = DB::table('parcelas')->get();
       
       return $this->crearRespuesta($parcelas, 200);
  
     
                        
   }
}
