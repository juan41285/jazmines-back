<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PlanoController extends Controller
{

      public function getCuadroById(Request $request){
        $parcela = $request->parcela;

     
          $cuadro = DB::table('plano')
                     ->join('parcelas','plano.sector','=', 'parcelas.nombre')
                     ->where('plano.id' , $parcela)
                     ->select('plano.*', 'parcelas.color', 'parcelas.contado', 'parcelas.id AS parcela_id')
                     ->first();
      //  $plano  = array('filas' =>  $filas, 'parcelas' => $cuadro );
 return $this->crearRespuesta($cuadro, 200);


   }
   public function getCuadro(Request $request){
        $cuadro = $request->cuadro;
        $manzana = $request->manzana;
          $filas = DB::select('select MAX(numero) AS total, fila, fila_letra, tipo, id from plano where manzana = '.$manzana.' and cuadro = '.$cuadro.' Group by fila, fila_letra');
          $cuadro = DB::table('plano')
                     ->join('parcelas','plano.sector','=', 'parcelas.nombre')
                     ->where('plano.manzana' , $manzana)
                     ->where('plano.cuadro' , $cuadro)
                     ->select('plano.*', 'parcelas.color', 'parcelas.contado', 'parcelas.id AS parcela_id')
                     ->get();
       $plano  = array('filas' =>  $filas, 'parcelas' => $cuadro );
 return $this->crearRespuesta($plano, 200);


   }
   public function datosPlanos(Request $request){
       $libres = DB::table('plano') ->count();
       $ocupados = DB::table('plano') ->where('estado' , 0)->count();
       $plano  = array('libre' => $libres , 'ocupado' => $ocupados);
      return $this->crearRespuesta($plano, 200);

   }
   public function generarMapa(Request $request){
   
    $tipo = $request->tipo;
    $cuadro = $request->cuadro;
    $manzana = $request->manzana;
    $fila = $request->fila;
    $fila_letra = $request->fila_letra;
    $cantidad = $request->cantidad;
    $sector = $request->sector;
   
    for ($i=1; $i <=$cantidad ; $i++) { 
        $parcela = DB::table('plano')->insertGetId([
                'sector' => $sector,
                'manzana' => $manzana,
                'cuadro' => $cuadro,
                'fila' => $fila,
                'parcela' => $i,
                'fila_letra' => $fila_letra,
                'numero' => $i,
                'tipo' => $tipo,
            ]);
    }
 
 return $this->crearRespuesta($parcela, 200);
   //M1-C2 - completo
   //M1-C3 - completo
   //M1-C1 - completo
   //M1-C4 - completo

   //M2-C1 - completo
   //M2-C2 - completo
   //M2-C3 - completo
   //M2-C4- completo

   //M3-C1- completo
   //M3-C3- completo
   //M3-C4- completo
   //M3-C2- completo

   //M4-C1- completo
   //M4-C2- completo
   //M4-C3- completo
   //M4-C4- completo

   //M5-C1- completo
   //M5-C2- completo
   //M5-C3- completo

   //M6-C1- completo


   } 
public function marcarOcupados(){

  $terrenos = DB::table('terrenos')->where('estado',1)->get();
  foreach ($terrenos as $terreno) {
    $plano_id = $terreno->plano_id;
    $ocupar = DB::table('plano')->where('id', $plano_id)->update(['estado' => 0]);

    # code...
  }
  return $terrenos;
}
 
}
