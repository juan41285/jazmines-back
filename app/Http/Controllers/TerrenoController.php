<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Terreno;
use App\Models\Pago;

class TerrenoController extends Controller
{

 public function setExpensas(Request $request){
    //  return $this->crearRespuesta($request->all(), 200); 
     $anios = $request->anios;
     $terreno_id = $request->terreno;
     $tipo = $request->tipo;

      $expensa = DB::table('expensas')->insertGetId(
                            [
                                'anios' => $anios,
                                'terreno_id' => $terreno_id,
                                'tipo' => $tipo,
                                
                            ]
                        );

                        return $this->crearRespuesta($expensa, 200); 
 }
 public function registrarInhumado(Request $request){
  
        $nombre = $request->nombre;
        // $dni = $request->dni;
        // $fecha_inhumacion = $request->fecha_inhumacion;
        // $fecha_nacimiento = $request->fecha_nacimiento;
        // $fecha_fallecimiento = $request->fecha_fallecimiento;
               if($request->exists('dni')){
                  $dni = $request->dni;

                 }else{
                   $dni = '00000000';
                  }
        if($request->exists('fecha_inhumacion')){
$fecha_inhumacion = $request->fecha_inhumacion;

        }else{
$fecha_inhumacion = '0000:00:00';

        }
        if ($request->exists('fecha_nacimiento')) {
    $fecha_nacimiento = $request->fecha_nacimiento;

} else {
    $fecha_nacimiento = '0000:00:00';

}
if ($request->exists('fecha_fallecimiento')) {
    $fecha_fallecimiento = $request->fecha_fallecimiento;

} else {
    $fecha_fallecimiento = '0000:00:00';

}


        $terreno = $request->terreno['terreno'];
        $ocupantes = $request->terreno['ocupantes'];
        $espacios = $request->terreno['parcela']['espacios'];
        $terreno_id = $terreno['id'];
// return $this->crearRespuesta($espacios, 200);   
        
$lugar = (intval($espacios) - intval($ocupantes));
// return $this->crearRespuesta($lugar, 200);   
if($lugar > 0){
     $ocupante = DB::table('inhumaciones')->insertGetId(
                            [
                                'nombre' => $nombre,
                                'dni' => $dni,
                                'fecha_inhumacion' => $fecha_inhumacion,
                                'fecha_nacimiento' => $fecha_nacimiento,
                                'fecha_fallecimiento' => $fecha_fallecimiento,
                                'terreno_id' => $terreno_id,
                                
                            
                            ]
                        );

                  return $this->crearRespuesta($ocupante, 200);        
                
                }else{
                     return $this->crearRespuesta(0, 200);   
                }
 }   

  public function eliminarInhumado(Request $request){
  
        $id = $request->id;
       
       

     $ocupante = DB::table('inhumaciones')->where('id', $id)->update(
                            [
                                
                                'estado' => 0,
                                
                                
                            
                            ]
                        );

                  return $this->crearRespuesta($id, 200);        
                
             
 }   
  public function editarInhumado(Request $request){
  
        $id = $request->id;
        $nombre = $request->nombre;
        $dni = $request->dni;
        $fecha_inhumacion = $request->fecha_inhumacion;
        $fecha_nacimiento = $request->fecha_nacimiento;
        $fecha_fallecimiento = $request->fecha_fallecimiento;
       

     $ocupante = DB::table('inhumaciones')->where('id', $id)->update(
                            [
                                'nombre' => $nombre,
                                'dni' => $dni,
                                'fecha_inhumacion' => $fecha_inhumacion,
                                'fecha_nacimiento' => $fecha_nacimiento,
                                'fecha_fallecimiento' => $fecha_fallecimiento,
                                
                                
                            
                            ]
                        );

                  return $this->crearRespuesta($id, 200);        
                
             
 }   
 public function bajaTerreno(Request $request){
      
    $id = $request->id; 
    $motivo_baja = $request->motivo_baja; 
   $fecha_baja = date("Y-m-d");

    $terreno = DB::table('terrenos')
            ->where('id', $id)
            ->update([
                'estado' => 0, 
                'motivo_baja' => $motivo_baja, 
                'fecha_baja' => $fecha_baja
            
            ]);

   $cliente_id = $request->cliente_id;
    $terrenos = DB::table('terrenos')
            ->where('terrenos.cliente_id', $cliente_id)
            ->where('terrenos.estado', 1)
            ->get();

    foreach ($terrenos as $terreno) {
        $parcela_id = $terreno->parcela_id;
        $terreno_id = $terreno->id;

        $parcela = DB::table('parcelas')
                   ->where('id', $parcela_id)
                   ->first();

        $ocupantes = DB::table('inhumaciones')
                        ->where('terreno_id', $terreno_id)
                        ->where('estado', 1)
                        ->get();  
        $expensas = DB::table('expensas')
                        ->where('terreno_id', $terreno_id)
                        ->first();  
         $resultTerrenos[] = ['terreno' => $terreno, 'parcela' => $parcela, 'ocupantes' => $ocupantes, 'expensas' => $expensas];                 
    }        

      return $this->crearRespuesta($resultTerrenos, 200);      
 } 

 public function getTerreno(Request $request){

    $id = $request->id;
    $terrenos = DB::table('terrenos')
            ->where('terrenos.cliente_id', $id)
            ->where('terrenos.estado', 1)
            ->get();

    foreach ($terrenos as $terreno) {
        $parcela_id = $terreno->parcela_id;
        $plano_id = $terreno->plano_id;
        $terreno_id = $terreno->id;

        $parcela = DB::table('parcelas')
                   ->where('id', $parcela_id)
                   ->first();
$plano = DB::table('plano')
                   ->where('id', $plano_id)
                   ->first();
        $ocupantes = DB::table('inhumaciones')
                        ->where('terreno_id', $terreno_id)
                        ->where('estado', 1)
                        ->get();  
       $expensas = DB::table('expensas')
                        ->where('terreno_id', $terreno_id)
                        ->first();                          
        
         $resultTerrenos[] = ['terreno' => $terreno, 'parcela' => $parcela, 'plano' => $plano, 'ocupantes' => $ocupantes, 'expensas' => $expensas];                 
    }        
   if(count($terrenos)){
return $this->crearRespuesta($resultTerrenos, 200);   
   }else{
return $this->crearRespuesta([], 200);   
   }
         
 }


 public function agregarTerreno(Request $request){
      
                    // $fv = date('Y-m-d',strtotime('+'.($j + 1).' month',strtotime($vencimiento)));)
         $cliente_id = $request->cliente_id;
         $parcela_id = $request->parcela_id;
         $pago_tipo = $request->pago_tipo;
         $contado = $request->contado;
         $cuotas = intval($request->cuotas);
         $plano_id = $request->id;
         $anios = 35;

         $parcela = DB::table('parcelas')->where('id',$parcela_id)->first();
         $cuota6 = $parcela->cuota6;
         $cuota12 = $parcela->cuota12;
         
        switch ($cuotas) {
            case 1:
                $parcela_precio = $contado;
                break;
            case 6:
                $parcela_precio =((($contado * $cuota6) / 100) + $contado) / 6;
                break;
            case 12:
            $parcela_precio =((($contado * $cuota12) / 100) + $contado) / 12;
            break;
            
         
        }
         $terreno = DB::table('terrenos')->insertGetId(
                            [
                        
                                'parcela_id' => $parcela_id,
                                'plano_id' => $plano_id,
                                'cuotas' => $cuotas,
                                'pago_tipo' => $pago_tipo,
                                'contado' => $contado,
                                'anios' => $anios,
                                'cliente_id' => $cliente_id,
                                'precio_cuota' => $parcela_precio,
                            
                            ]
                        );
            if($terreno){
                $ocupar = DB::table('plano')->where('id', $plano_id)->update(['estado' => 0]);
                for ($i=0; $i < $cuotas; $i++) { 
                    $concepto = 'Pago cuota terreno ('.($i+1).' de '.$cuotas.')';
                    $vencimiento = date("Y-m-d");
                    $dias = $i * 30;
                    $fv = date('Y-m-d',strtotime('+'.$dias.' days',strtotime($vencimiento)));
                    $pago = DB::table('pagos')->insertGetId(
                                        [
                                            'cliente_id' => $cliente_id,
                                            'concepto' => $concepto,
                                            'monto' => $parcela_precio,
                                            'terreno_id' => $terreno,
                                            'vencimiento' => $fv
                                        ]
                                    );
                }
                 for ($j=0; $j < 420; $j++) { 
                    $concepto = 'Pago mensual de mantenimiento y expensas';
                    $mes=date('m'); $aniov=date('Y');
                   $vencimiento = date('Y-m-d',strtotime($aniov.'-'.$mes.'-'.'15'));
                    $fv = date('Y-m-d',strtotime('+'.($j + 1).' month',strtotime($vencimiento)));

                    $pago = DB::table('pagos')->insertGetId(
                                        [
                                            'cliente_id' => $cliente_id,
                                            'concepto' => $concepto,
                                            'monto' => 0,
                                            'terreno_id' => $terreno,
                                            'vencimiento' => $fv
                                        ]
                                    );
                }
                $this->setPagosCliente($cliente_id);



            }                           
            // $terrenos = DB::table('terrenos')
            //             ->where('cliente_id', $cliente_id)
            //             ->get();

                return $this->crearRespuesta($terreno, 200);   
            }         
  
  public function setPagosCliente($id){
      
    // $id = $request->id; 
    $terrenos = DB::table('terrenos')->where('cliente_id', $id)->get(); 
    foreach ($terrenos as $terreno) {
        $terreno_id = $terreno->id;
        $parcela_id = $terreno->parcela_id;
        $tipo = DB::table('expensas')->select('tipo')->where('terreno_id',$terreno_id)->first();
        if($tipo){
         $mantenimiento = 'mantenimiento_'.$tipo->tipo;
         $parcela = DB::table('parcelas')->where('id', $parcela_id)->first();
         $costo = $parcela->$mantenimiento;
         $pagos = DB::table('pagos')
                    ->where('cliente_id', $id)
                    ->where('concepto','like','Pago mensual de mantenimiento y expensas')
                    ->update([
                        'monto' => $costo
                    ]);
        }else{
            
   $parcela = DB::table('parcelas')->where('id', $parcela_id)->first();
         $costo = $parcela->mantenimiento_comun;
         $pagos = DB::table('pagos')
                    ->where('cliente_id', $id)
                    ->where('concepto','like','Pago mensual de mantenimiento y expensas')
                    ->update([
                        'monto' => $costo
                    ]);
        }

        # code...
    }
    // $pagos = DB::table('pagos')
    //                 ->where('cliente_id', $id)
    //                 ->where('estado',0)
    //                 ->orderBy('estado', 'DESC')
    //                 ->paginate(20);

   return true;                      
   
 } 
   
}
