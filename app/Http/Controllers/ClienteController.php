<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Terreno;
use App\Models\Pago;

class ClienteController extends Controller
{
     
 public function agregarTerreno(Request $request){
         $cliente_id = $request->cliente_id;
         $cuadro = $request->cuadro;
         $cuotas = $request->cuotas;
         $fila = $request->fila;
         $manzana = $request->manzana;
         $pago_tipo = $request->pago_tipo;
         $parcela = $request->parcela;
         $parcela_id = $request->parcela_id;
         switch ($cuotas) {
             case 1:
                  $parcela_precio = $parcela_id['contado'];
                 break;
             case 6:
                  $parcela_precio = $parcela_id['cuota6'];
                 break;
            case 12:
                  $parcela_precio = $parcela_id['cuota12'];
                 break;     
             
         }
        
         $parcela_fk = $parcela_id['id'];
         $excavacion = $parcela_id['excavacion'];
         $mantenimiento = $parcela_id['mantenimiento'];
         $nombre = $parcela_id['nombre'];

  $terreno = DB::table('terrenos')->insertGetId(
                            [
                                'sector' => $nombre,
                                'manzana' => $manzana,
                                'cuadro' => $cuadro,
                                'fila' => $fila,
                                'parcela_id' => $parcela_fk,
                                'parcela' => $parcela,
                                'cuotas' => $cuotas,
                                'pago_tipo' => $pago_tipo,
                                'precio' => $parcela_precio,
                                'cliente_id' => $cliente_id
                            
                            ]
                        );
$clientes = Cliente::with(['pagos','terrenos'])
                       ->where('id', $cliente_id)
                       ->first();

    return $this->crearRespuesta($clientes, 200);
 }
   public function getClientes(Request $request){

//    $cliente_id = DB::table('terrenos')
//                             ->join('inhumaciones','inhumaciones.terreno_id', 'terrenos.id')
//                             ->where('inhumaciones.nombre', 'like' ,'%'.$buscar.'%')
//                             ->select('terrenos.cliente_id')
//                             ->paginate(10);
    
    if($request->exists('buscar') && $request->buscar){
         $buscar = $request->buscar;
          $terreno_id = DB::table('terrenos')
                            ->join('inhumaciones','inhumaciones.terreno_id', 'terrenos.id')
                            ->where('nombre', 'like' ,'%'.$buscar.'%')
                            ->select('terrenos.cliente_id AS id')
                            ->first();

                            
          if($terreno_id ){
              $cliente_id = $terreno_id->id;
                    $clientes = Cliente::with(['pagos','terrenos'])
                                ->where('id', $cliente_id)
                                ->orderBy('id','DESC')
                                ->paginate(10);
          }else{
              $clientes = Cliente::with(['pagos','terrenos'])
                                ->where('nombre', 'like' ,'%'.$buscar.'%')
                                ->orWhere('dni', 'like', '%'.$buscar.'%')
                                ->orderBy('id','DESC')
                                ->paginate(10);
          }                  
         
    }else{
        $clientes = Cliente::with(['pagos','terrenos'])->orderBy('id','DESC')->paginate(10);
    }

    return $this->crearRespuesta($clientes, 200);
       
   } 
    public function getCliente(Request $request){

         $id = $request->id;
         $clientes = Cliente::with(['pagos','terrenos'])
                       ->where('id', $id)
                       ->first();

    return $this->crearRespuesta($clientes, 200);
       
   } 
   public function guardarCliente(Request $request){
       
       $nombre = $request->nombre;
       $dni = $request->dni;
       $localidad = $request->localidad;
       $provincia = $request->provincia;
       $cp = $request->cp;
       $ingreso = $request->ingreso;
       $telefono = $request->telefono;
       $direccion = $request->direccion;
       if($request->exists('ocupacion')){
$ocupacion = $request->ocupacion;
       }else{
$ocupacion = NULL;
       }
       

       $existe = DB::table('clientes')
            ->where('dni', '=', $dni)
            ->first();
       
       if(count($existe)){
return $this->crearRespuesta($existe, 200);
       }
       else{
           $cliente = DB::table('clientes')->insertGetId(
                            [
                                'nombre' => $nombre,
                                'dni' => $dni,
                                'localidad' => $localidad,
                                'provincia' => $provincia,
                                'cp' => $cp,
                                'ingreso' => $ingreso,
                                'telefono' => $telefono,
                                'ocupacion' => $ocupacion,
                                'direccion' => $direccion
                            
                            ]
                        );

    if($cliente){
          $nuevo = DB::table('clientes')
            ->where('id', '=', $cliente)
            ->first(); 
           return $this->crearRespuesta($nuevo, 201);
       }
       else{
           return $this->crearRespuesta(0, 500);
       }
       }
    //    $cliente = DB::table('clientes')->insertGetId(
    //                         [
    //                             'nombre' => $nombre,
    //                             'dni' => $dni,
    //                             'localidad' => $localidad,
    //                             'provincia' => $provincia,
    //                             'cp' => $cp,
    //                             'ingreso' => $ingreso,
    //                             'telefono' => $telefono,
    //                             'direccion' => $direccion
                            
    //                         ]
    //                     );
     
                        
   }
   public function editarCliente(Request $request){
       
       $id = $request->id;
       $nombre = $request->nombre;
       $dni = $request->dni;
       $localidad = $request->localidad;
       $provincia = $request->provincia;
       $cp = $request->cp;
       $ingreso = $request->ingreso;
       $telefono = $request->telefono;
       $direccion = $request->direccion;
       if($request->exists('ocupacion')){
$ocupacion = $request->ocupacion;
       }else{
$ocupacion = NULL;
       }
       

   
   
           $cliente = DB::table('clientes')->where('id', $id)->update(
                            [
                                'nombre' => $nombre,
                                'dni' => $dni,
                                'localidad' => $localidad,
                                'provincia' => $provincia,
                                'cp' => $cp,
                                'ingreso' => $ingreso,
                                'telefono' => $telefono,
                                'ocupacion' => $ocupacion,
                                'direccion' => $direccion
                            
                            ]
                        );

   
           return $this->crearRespuesta($id, 201);
   
       

     
                        
   }
}
