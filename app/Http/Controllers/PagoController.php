<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Terreno;
use App\Models\Pago;
use PDF;
class PagoController extends Controller
{

    public function generarPago(Request $request){
     $concepto = $request->concepto; 
     $vencimiento = $request->vencimiento; 
     $monto = $request->monto; 
     $cliente_id = $request->cliente_id; 
     $anulado = date('Y-m-d');
    $terreno = DB::table('terrenos')->where('cliente_id', $cliente_id)->first();
    $terreno_id = $terreno->id;
       $pago =  DB::table('pagos')->insertGetId(
                            [
                                
                                'concepto' => $concepto,
                                'monto' => $monto,
                                'vencimiento' => $vencimiento,
                                'cliente_id' => $cliente_id,
                                'terreno_id' => $terreno_id
                            
                            ]
                        );

       
    return $this->crearRespuesta($pago, 200);   
}
public function anularPago(Request $request){
 $id = $request->id; 
     $motivo = $request->motivo; 
    $anulado = date('Y-m-d');

       $terreno = DB::table('pagos')
            ->where('id', $id)
            ->update([
                'estado' => -1, 
                'motivo' => $motivo,
                'anulado' => $anulado
            
            ]);

       
    return $this->crearRespuesta($motivo, 200);   
}
 public function registrarPago(Request $request){
     $id = $request->id; 
     $cliente_id = $request->cliente_id; 
     $fecha_pago = $request->fecha_pago; 
     $vencimiento = $request->vencimiento; 
     $monto = $request->monto; 
     $concepto = $request->concepto; 
     $estado = 1;

     $cliente = DB::table('clientes')->where('id', $cliente_id)->first();
     $nombre = $cliente->nombre;
     $dni = $cliente->dni;

 $html = '<style type="text/css" media="all">
body
{
  size: 21cm 29.7cm;
   margin: 0px ;
   font-family:Arial, Helvetica, sans-serif;

}

@page {
  size: 21cm 29.7cm;
   margin: 0px ;


}
.content-firma{
    width:18cm;
margin:50px auto;
margin-top: 100px;
}
.firma{
    width: 40%;
    float: left;
    margin:0 auto;
    text-align: center;
    border-top:3px dotted #ddd ;
}
.table{
border-collapse: collapse;
font-size: 14px;
text-align: left;
width:18cm;
margin:30px auto;
}
.table tr > th, .table tr > td {
    border:1px solid #f1f1f1;
    padding: 6px;
    
}
.page {
  size: 21cm 29.7cm;
   margin: 0px ;


}
.tabla-li{
  list-style-type: none;
  margin:0;
  padding: 0;
  width: 80%;
  display:block;
}
.tabla-li li {
  padding: 10px;
  border-bottom: 1px solid #f1f1f1;
}
.logo{
  position: absolute;
  right: 0;
  top:0;
  height:100px;
  width: 40%;

}
.logo img{
     width: 250px;
  padding: 10px;
  display: block;
  margin-top: 10px;
}
.titulo{
    width: 50%;
    height:100px;
}   
.titulo h1{
    font-size:22px;
    margin: 20px;
    padding: 30px;
   
}                    </style>
                   
    <div class="page">
          <div class="titulo"><h1>COMPROBANTE DE PAGO</h1></div>
          <div class="logo"><img src="http://localhost/logo.png" alt=""></div>
          <table class="table">
           <tr>
                  <th>Recibo N°</th>
                  <td colspan="3">'.$id.'</td>
              </tr>
              <tr>
                  <th>Cliente</th>
                  <td>'.$nombre.'</td>
              
                  <th>DNI</th>
                  <td>'.$dni.'</td>
              </tr>
              <tr>
                  <th>Concepto</th>
                  <td colspan="3">'.$concepto.'</td>
              </tr>
              <tr>
                  <th>Vencimiento</th>
                  <td>'.date('d/m/Y', strtotime($vencimiento)).'</td>
                   <th>Fecha de Pago</th>
                  <td>'.date('d/m/Y', strtotime($fecha_pago)).'</td>
              </tr>
               <tr>
                  <th style="font-size:20px; padding:15px 10px; background:#f1f1f1">Monto</th>
                  <th style="font-size:20px; padding:15px 10px; background:#f1f1f1" colspan="3">$'.$monto.'</th>
              </tr>
          </table>
          <div class="content-firma">
 <div class="firma">
              <h3>Firma del cliente</h3>
          </div>
          <div class="firma" style="float:right">
              <h3>Firma de la concesionaria</h3>
          </div>
          </div>
         
    </div>


';
  $terreno = DB::table('pagos')
            ->where('id', $id)
            ->update([
                'estado' => 1, 
                'fecha_pago' => $fecha_pago,
                'comprobante' => $html
            
            ]);
//   PDF::loadHTML($html)->setPaper('A4')
//                             ->setOrientation('portrait')
//                              ->setOption('margin-top', 10)
//                              ->setOption('margin-left', 10)
//                              ->setOption('margin-right', 10)
//                              ->setOption('margin-bottom', 10)
//                              ->save('pdf/pagos/comprobante#'.$id.'.pdf');
//  return $this->crearRespuesta($nombrePdf, 200);
       
    return $this->crearRespuesta($html, 200);   
 }   
 public function getPagosCliente(Request $request){
      
    $id = $request->id; 
if($request->exists('fecha')){
$fecha = $request->fecha;

$mes = date('m', strtotime($fecha));
$anio = date('Y', strtotime($fecha));

}else{
   $mes = date('m');
$anio = date('Y');
 
}


    $pagos = DB::table('pagos')
    ->join('clientes', 'clientes.id', 'pagos.cliente_id')
    ->join('terrenos', 'terrenos.id', 'pagos.terreno_id')
    ->whereMonth('pagos.vencimiento', '<=', $mes)
    ->whereYear('pagos.vencimiento','<=', $anio)
    ->where('pagos.cliente_id', $id)
    ->select('pagos.id','pagos.monto', 'pagos.concepto', 'pagos.vencimiento', 'pagos.estado', 'clientes.nombre', 'clientes.dni', 'clientes.id AS cliente_id', 'terrenos.id AS terreno')
    ->orderBy('vencimiento', 'DESC')
    ->paginate(15);


   return $this->crearRespuesta($pagos, 200);                      
   
 } 
public function getDeudaCliente(Request $request){
      
    $id = $request->id; 
if($request->exists('fecha')){
$fecha = $request->fecha;

$mes = date('m', strtotime($fecha));
$anio = date('Y', strtotime($fecha));

}else{
   $mes = date('m');
$anio = date('Y');
 
}


    $pagos = DB::table('pagos')
    ->join('clientes', 'clientes.id', 'pagos.cliente_id')
    ->whereMonth('pagos.vencimiento', '<=', $mes)
    ->whereYear('pagos.vencimiento','<=', $anio)
    ->where('pagos.cliente_id', $id)
    ->where('pagos.estado',0)
    ->sum('pagos.monto');


   return $this->crearRespuesta($pagos, 200);                      
   
 } 

   public function anularTodos(){

$mes = date('m');
$anio = date('Y');

       $terreno = DB::table('pagos')
             ->whereMonth('vencimiento', '<=', $mes)
    ->whereYear('vencimiento','<=', $anio)
            ->update([
                'estado' => -1, 
                'motivo' => 'sistema'
            
            ]);

       
    return $this->crearRespuesta($anio, 200);   
}
}
