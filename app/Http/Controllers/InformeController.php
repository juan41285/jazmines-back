<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Terreno;
use App\Models\Pago;
use PDF;
class InformeController extends Controller
{

    public function deudaTotal(Request $request){

     $deudaTotal =  DB::table('pagos')
                  ->where('estado',0)
                  ->sum('monto');


     $fecha = $request->fecha; 
     $mes = date('m', strtotime($fecha));
     $anio = date('Y', strtotime($fecha));
    // return $this->crearRespuesta($anio, 200);   

     $deudaMensual =  DB::table('pagos')
                  ->where('estado',0)
                  ->whereMonth('vencimiento','<=', $mes)
                  ->whereYear('vencimiento', $anio)
                  ->sum('monto');
     
    $pagoMensual =  DB::table('pagos')
                  ->where('estado',1)
                  ->whereMonth('vencimiento','<=', $mes)
                  ->whereYear('vencimiento', $anio)
                  ->sum('monto');      
                          
              $pago  = array('deudaTotal' => $deudaTotal,'deudaMensual' => $deudaMensual,'pagoMensual' => $pagoMensual );    
    return $this->crearRespuesta($pago, 200);   
}
public function deudaMensual(Request $request){
     $fecha = $request->fecha; 
     $mes = date('m', strtotime($fecha));
     $anio = date('Y', strtotime($fecha));
$deudaMensual =  DB::table('pagos')
                  ->join('clientes','clientes.id','pagos.cliente_id')
                  ->whereMonth('pagos.vencimiento','<=', $mes)
                  ->whereYear('pagos.vencimiento', $anio)
                  ->select('pagos.monto', 'pagos.concepto', 'pagos.vencimiento', 'pagos.estado', 'clientes.nombre', 'clientes.dni', 'clientes.id')
                  ->orderBy('vencimiento', 'ASC')
                  ->paginate(25);

    return $this->crearRespuesta($deudaMensual, 200);   

}

   
}
