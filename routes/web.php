<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/test', function () use ($router) {
    return "juan";
});
//pagos
$router->post('/pagos/generar', 'PagoController@generarPago');
$router->post('/pagos/registrar', 'PagoController@registrarPago');
$router->post('/pagos/anular', 'PagoController@anularPago');
$router->post('/pagos/cliente', 'PagoController@getPagosCliente');
$router->post('/pagos/deuda', 'PagoController@getDeudaCliente');
$router->get('/pagos/anular/todos', 'PagoController@anularTodos');

//

//terrrenos
$router->post('/terrenos/expensas', 'TerrenoController@setExpensas');
$router->post('/terrenos/registrar/inhumado', 'TerrenoController@registrarInhumado');
$router->post('/terrenos/editar/inhumado', 'TerrenoController@editarInhumado');
$router->post('/terreno/eliminar/inhumado', 'TerrenoController@eliminarInhumado');
$router->post('/terrenos', 'TerrenoController@getTerreno');
$router->post('/terrenos/agregar', 'TerrenoController@agregarTerreno');
$router->post('/terrenos/baja', 'TerrenoController@bajaTerreno');

//clientes
$router->post('/clientes', 'ClienteController@getClientes');
$router->post('/cliente/detalle', 'ClienteController@getCliente');
$router->post('/cliente/guardar', 'ClienteController@guardarCliente');
$router->post('/cliente/editar', 'ClienteController@editarCliente');


//parcelas
$router->post('/parcelas/gastos', 'ParcelasController@guardarGastos');
$router->post('/parcelas/editar', 'ParcelasController@editarParcelas');
$router->get('/parcelas', 'ParcelasController@getParcelas');

//generador plani
$router->get('/plano/marcar', 'PlanoController@marcarOcupados');

$router->get('/plano/totales', 'PlanoController@datosPlanos');
$router->post('/plano/cuadro/id', 'PlanoController@getCuadroById');
$router->post('/plano/cuadro', 'PlanoController@getCuadro');
$router->post('/plano/generar', 'PlanoController@generarMapa');

//informes

$router->post('/informe/mes/cobrado', 'InformeController@pagosMensual');
$router->post('/informe/mes', 'InformeController@deudaMensual');
$router->post('/informe/deuda', 'InformeController@deudaTotal');

//cobradores
$router->post('/cobradores/guardar', 'CobradorController@addCobrador');
$router->get('/cobradores', 'CobradorController@getCobradores');
